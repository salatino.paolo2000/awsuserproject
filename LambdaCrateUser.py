import json
import uuid
import boto3

# Creazione di una funzione Lambda che si connette al DynamoDB 
# alla tabella Users per inserire un nuovo utente con le proprietà 
# definite nella request
def lambda_handler(event, context):
    # recupero proprietà utente 
    id = str(uuid.uuid4())
    name = event['name']
    surname = event['surname']
    email  = event['email']
    password = event['password']
    city = event['city']

    # connessione al DB e inserimeto di un elemento  
    Dynamodb = boto3.resource(
        'dynamodb',
        aws_access_key_id = 'AKIATM2HMAPE2OII6S5C',
        aws_secret_access_key = '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',
        region_name = 'us-east-1'
    )
    table = Dynamodb.Table('Users')
    response = table.put_item(
        Item = {
            'id':id,
            'name':name,
            'surname':surname,
            'email':email,
            'password':password,
            'city':city
        }
    )

    # restituzione dell'esito della chiamata 
    return {
        'statusCode': 200,
        'body': json.dumps('Nuovo utente inserito con id ' + str(id))
    }
