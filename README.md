++++Guida Utente++++

-Creazione Utente-
Per creare un nuovo utente è necessario effettuare una chiamata HTTPS Post tramite un applicativo come Postman all’indirizzo web specificato nella proprietà “host” del file swagger_user.yml fornito all’interno della cartella “Documentazione” presente su GitLab.

Esempio di chiamata: 
https://ucv4e8znx7.execute-api.us-east-1.amazonaws.com/roduzione/createuser 

Per quanto riguarda il body da inserire nella request si può sempre fare riferimento al file YAML su citato.


-Lettura Utente-
Per recuperare un utente già esistente è necessario effettuare una chiamata HTTPS Get  tramite un qualsiasi browser al solito indirizzo web specificato nel file YAML.

Esempio di chiamata:
https://ucv4e8znx7.execute-api.us-east-1.amazonaws.com/roduzione/getuserbyid?id=[id utente da recuperare]

Il valore del parametro “id” può essere recuperato dal body della response della chiamata all’Api Rest createuser.
  